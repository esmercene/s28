


// --------Activity No. 3 - insertOne Method-------
db.rooms.insert({
	name: "single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
})

// --------Activity No. 4 - insertMany Method-------
db.rooms.insertMany([
   {
	name: "double",
	accomodates: 3,
	price: 2000,
	description: "A room fit for a small family going on a vacation",
	rooms_available: 5,
	isAvailable: false
   },
   {
   	name: "queen",
	accomodates: 4,
	price: 4000,
	description: "A room with a queen sized bed perfect for a simple getaway",
	rooms_available: 15,
	isAvailable: false
   }
])

// -------Activity No.5 - search a room with the name double--------
db.rooms.find({name: "double"})

// Result
// {
//     "_id" : ObjectId("63340d4964aa7e6e1b4ebc36"),
//     "name" : "double",
//     "accomodates" : 3.0,
//     "price" : 2000.0,
//     "description" : "A room fit for a small family going on a vacation",
//     "rooms_available" : 5.0,
//     "isAvailable" : false
// }



// -------Activity No. 6 - update the queen room and set to  available rooms to 0
db.rooms.updateOne(
	{
		name: "queen"
	},
	{
		$set: {
			rooms_available: 0
		}
	}
)

// Result

// {
//     "_id" : ObjectId("63340d4964aa7e6e1b4ebc37"),
//     "name" : "queen",
//     "accomodates" : 4.0,
//     "price" : 4000.0,
//     "description" : "A room with a queen sized bed perfect for a simple getaway",
//     "rooms_available" : 0.0,
//     "isAvailable" : false
// }


// -------Activity No.7 - delete all rooms that have 0 rooms available
db.rooms.deleteOne({
	rooms_available: 0
})



